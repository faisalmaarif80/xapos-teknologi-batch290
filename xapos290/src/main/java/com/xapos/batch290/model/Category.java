package com.xapos.batch290.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "category")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Category {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "category_code")
	private String categoryCode;
	
	@Column(name = "category_name")
	private String categoryName;
	
	@Column(name = "is_active")
	private Boolean isActive;
	
	@Column(name = "created_by")
	private String createdBy;
	
	@Column(name = "created_date")
	private Date createdDate;
	
	@Column(name = "modify_by")
	private String modifyBy;
	
	@Column(name = "modify_date")
	private Date modifyDate;

//	@OneToMany(mappedBy = "category")
//	private Set<Variant> variants = new LinkedHashSet<>();
//

	public Category(String categoryName) {
		this.categoryName = categoryName;
	}
}
