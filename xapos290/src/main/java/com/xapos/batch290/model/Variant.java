package com.xapos.batch290.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "variant")
public class Variant {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;

	@Column(name="variant_code")
	private String variantCode;

	@Column(name="variant_name")
	private String variantName;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modify_by")
	private String modifyBy;

	@Column(name = "modify_date")
	private Date modifyDate;

	@Column(name = "category_id")
	private Long categoryId;

	@ManyToOne
	@JoinColumn(name = "category_id" , insertable = false, updatable = false)
	private Category category;

}

