package com.xapos.batch290.controller;
import com.xapos.batch290.model.OrderHeader;
import com.xapos.batch290.repository.OrderHeaderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiOrderHeaderController {

    @Autowired
    public OrderHeaderRepository orderHeaderRepository;

    @PostMapping("orderheader")
    public ResponseEntity<Object> createHeader(@RequestBody OrderHeader orderHeader) {
        String timeDec = System.currentTimeMillis() + "";
        orderHeader.setReference(timeDec);

        OrderHeader orderHeaderData = this.orderHeaderRepository.save(orderHeader);

        if(orderHeaderData.equals(orderHeader)){
            return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
    } else {
        return new ResponseEntity<Object>("Save Failed",HttpStatus.NO_CONTENT);
    }
    }

    @GetMapping("orderheadermaxid")
    public ResponseEntity<Long> getMaxOrderHeader(){
        try {
            Long orderHeader = this.orderHeaderRepository.findByMaxId();
            return new ResponseEntity<Long>(orderHeader, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("done/orderheader")
    public ResponseEntity<Object> doneProccess(@RequestBody OrderHeader orderHeader){
        Long id = orderHeader.getId();
        Optional<OrderHeader> orderHeaderData = this.orderHeaderRepository.findById(id);

        if(orderHeaderData.isPresent()){
            orderHeader.setId(id);
            this.orderHeaderRepository.save(orderHeader);
            return new ResponseEntity<Object>("Order Successfully", HttpStatus.OK);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("showorderheader")
    public ResponseEntity<List<OrderHeader>> getAllOrderHeader(){
        try {
            List<OrderHeader> orderHeader = this.orderHeaderRepository.findByAmount();
            return new ResponseEntity<List<OrderHeader>>(orderHeader, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<List<OrderHeader>>(HttpStatus.NO_CONTENT);
        }
    }

}
