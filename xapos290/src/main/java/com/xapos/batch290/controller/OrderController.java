package com.xapos.batch290.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/order/")
public class OrderController {

    @GetMapping("indexapi")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("orders/indexapi");
        return view;
    }
}
