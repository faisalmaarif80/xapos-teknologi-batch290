package com.xapos.batch290.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import com.xapos.batch290.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos.batch290.model.Product;
import com.xapos.batch290.model.Variant;
import com.xapos.batch290.repository.CategoryRepository;
import com.xapos.batch290.repository.ProductRepository;
import com.xapos.batch290.repository.VariantRepository;

@Controller
@RequestMapping("/product/")
public class ProductController {
	
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private VariantRepository variantRepository;
	
	@GetMapping("index")
	private ModelAndView index() {
		ModelAndView view = new ModelAndView("/product/index");
		List<Product> listProduct = this.productRepository.findAll();
		view.addObject("listProduct", listProduct);
		return view;
	}
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("/product/indexapi");
		return view;
	}
	
	@GetMapping("addform")
	public ModelAndView addform() {
		ModelAndView view = new ModelAndView("/product/addform");
		List<Category> listCategories = this.categoryRepository.findAll();
		view.addObject("listCategory", listCategories);
		
		List<Variant> listVariant = this.variantRepository.findAll();
		view.addObject("listVariant", listVariant);

		Product product = new Product();
		view.addObject("product", product);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Product product, BindingResult result) {
		if (!result.hasErrors()) {
			if (product.getId() == null) {
				product.setCreatedBy("user1");
				product.setCreatedDate(Date.from(Instant.now()));
			} else {
				Product products = this.productRepository.findById(product.getId()).orElse(null);
				if (products != null) {
					product.setCreatedBy(products.getCreatedBy());
					product.setCreatedDate(products.getCreatedDate());
					product.setModifyBy("user1");
					product.setModifyDate(Date.from(Instant.now()));
				}
			}
			this.productRepository.save(product);
			return new ModelAndView("redirect:/product/index");
		} else {
			return new ModelAndView("redirect:/product/index");
		}
	}

	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("product/addform");
		List<Category> listCategories = this.categoryRepository.findAll();
		view.addObject("listCategory", listCategories);
		
		List<Variant> listVariant = this.variantRepository.findAll();
		view.addObject("listVariant", listVariant);
		
		Product product = this.productRepository.findById(id).orElse(null);
		view.addObject("product", product);
		return view;
	}

	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		if (id != null) {
			this.productRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/product/index");
	}
}
