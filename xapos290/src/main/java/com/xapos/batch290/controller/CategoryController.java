package com.xapos.batch290.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos.batch290.model.Category;
import com.xapos.batch290.repository.CategoryRepository;

@Controller
@RequestMapping("/category/")
public class CategoryController {

	@Autowired
	private CategoryRepository categoryRepository;

	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/category/index");
		List<Category> listCategories = this.categoryRepository.findAll();
		view.addObject("listCategory", listCategories);
		return view;
	}
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("/category/indexapi");
		return view;
	}
	
	

	@GetMapping("addform")
	public ModelAndView addform() {
		ModelAndView view = new ModelAndView("/category/addform");
		Category category = new Category();
		view.addObject("category", category);
		return view;
	}

	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Category category, BindingResult result) {
		if (!result.hasErrors()) {
			if (category.getId() == null) {
				category.setCreatedBy("user1");
				category.setCreatedDate(Date.from(Instant.now()));
			} else {
				Category categories = this.categoryRepository.findById(category.getId()).orElse(null);
				if (categories != null) {
					category.setCreatedBy(categories.getCreatedBy());
					category.setCreatedDate(categories.getCreatedDate());
					category.setModifyBy("user1");
					category.setModifyDate(Date.from(Instant.now()));
				}
			}
			this.categoryRepository.save(category);
			return new ModelAndView("redirect:/category/index");
		} else {
			return new ModelAndView("redirect:/category/index");
		}
	}

	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("category/addform");
		Category category = this.categoryRepository.findById(id).orElse(null);
		view.addObject("category", category);
		return view;
	}

	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		if (id != null) {
			this.categoryRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/category/index");
	}
}
