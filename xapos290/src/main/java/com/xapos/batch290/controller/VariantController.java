package com.xapos.batch290.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import com.xapos.batch290.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos.batch290.model.Variant;
import com.xapos.batch290.repository.CategoryRepository;
import com.xapos.batch290.repository.VariantRepository;

@Controller
@RequestMapping("/variant/")
public class VariantController {

	@Autowired
	private VariantRepository variantRepository;
	@Autowired
	private CategoryRepository categoryRepository;

	
	
	@GetMapping("index")
	private ModelAndView index() {
		ModelAndView view = new ModelAndView("/variant/index");
		List<Variant> listVariant = this.variantRepository.findAll();
		view.addObject("listVariant", listVariant);
		return view;
	}
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("/variant/indexapi");
		return view;
	}
	
	@GetMapping("addform")
	public ModelAndView addform() {
		ModelAndView view = new ModelAndView("/variant/addform");
		List<Category> listCategories = this.categoryRepository.findAll();
		view.addObject("listCategory", listCategories);

		Variant variant = new Variant();
		view.addObject("variant", variant);
		return view;
	}

	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Variant variant, BindingResult result) {
		if (!result.hasErrors()) {
			if (variant.getId() == null) {
				variant.setCreatedBy("user1");
				variant.setCreatedDate(Date.from(Instant.now()));
			} else {
				Variant varians = this.variantRepository.findById(variant.getId()).orElse(null);
				if (varians != null) {
					variant.setCreatedBy(varians.getCreatedBy());
					variant.setCreatedDate(varians.getCreatedDate());
					variant.setModifyBy("user1");
					variant.setModifyDate(Date.from(Instant.now()));
				}
			}
			this.variantRepository.save(variant);
			return new ModelAndView("redirect:/variant/index");
		} else {
			return new ModelAndView("redirect:/variant/index");
		}
	}

	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("variant/addform");
		List<Category> listCategories = this.categoryRepository.findAll();
		view.addObject("listCategory", listCategories);
		Variant variant = this.variantRepository.findById(id).orElse(null);
		view.addObject("variant", variant);
		return view;
	}

	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		if (id != null) {
			this.variantRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/variant/index");
	}

}
