package com.xapos.batch290.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeController {
	
	@GetMapping("form")
	public String form() {
		return "form";
	}
	
	@GetMapping("kalkulator")
	public String kalkulator() {
		return "kalkulator";
	}
}
