package com.xapos.batch290.controller;

import com.xapos.batch290.model.OrderDetail;
import com.xapos.batch290.repository.OrderDetailRepository;
import com.xapos.batch290.repository.OrderHeaderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiOrderDetailController {

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @PostMapping("orderdetail")
    public ResponseEntity<Object> saveOrderDetail(@RequestBody OrderDetail orderDetail){
        OrderDetail orderDetailData = this.orderDetailRepository.save(orderDetail);

        if (orderDetailData.equals(orderDetail)){
            return new ResponseEntity<Object>("Save Item Success", HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>("Save Failed",HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("orderdetailbyorderheader/{headerId}")
    public ResponseEntity<List<OrderDetail>> getAllOrderDetailByOrderHeader(@PathVariable("headerId") Long headerId){
        try {
         List<OrderDetail> orderDetail = this.orderDetailRepository.findByHeaderId(headerId);
            return new ResponseEntity<List<OrderDetail>>(orderDetail, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<List<OrderDetail>>(HttpStatus.NO_CONTENT);
        }
    }





}

