package com.xapos.batch290.repository;

import com.xapos.batch290.model.OrderHeader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderHeaderRepository extends JpaRepository<OrderHeader, Long> {
    @Query ("SELECT MAX(id) FROM OrderHeader")
    public Long findByMaxId();

    @Query (value = "FROM OrderHeader WHERE amount > '0' ")
    public List<OrderHeader> findByAmount();

}
