package com.xapos.batch290.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xapos.batch290.model.OrderDetail;

import java.util.List;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long> {
    public List<OrderDetail> findByHeaderId(Long id);
}

