package com.xapos.batch290.repository;

import java.util.List;

import com.xapos.batch290.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CategoryRepository extends JpaRepository<Category, Long> {
	List<Category> findByIsActive(Boolean isActive);

	@Query("FROM Category WHERE Lower(categoryName) LIKE Lower(concat('%', ?1 , '%')) AND isActive=true")
	List<Category> searchByKeyword(String keyword);
}
