package com.miniproject.demo.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject.demo.model.EducationLevel;


public interface EducationLevelRepository extends JpaRepository<EducationLevel, BigInteger> {
	List<EducationLevel> findByIsDelete(Boolean isDelete);
	
	@Query("FROM EducationLevel WHERE Lower(name) LIKE Lower(concat('%', ?1 , '%')) AND isDelete=false")
	List<EducationLevel> searchByKeyword(String keyword);

}
