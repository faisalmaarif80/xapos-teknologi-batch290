package com.miniproject.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/profilesharedlayout/")
public class ProfileSharedLayoutController {

	@GetMapping("indexpasien")
	public ModelAndView indexpasien() {
		ModelAndView view = new ModelAndView("/profilesharedlayout/indexpasien");
		return view;
	}
	
	@GetMapping("indexdokter")
	public ModelAndView indexdokter() {
		ModelAndView view = new ModelAndView("/profilesharedlayout/indexdokter");
		return view;
	}
	
	@GetMapping("indexadmin")
	public ModelAndView indexadmin() {
		ModelAndView view = new ModelAndView("/profilesharedlayout/indexadmin");
		return view;
	}
}
