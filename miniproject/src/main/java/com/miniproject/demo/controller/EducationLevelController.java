package com.miniproject.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.miniproject.demo.repository.EducationLevelRepository;

@Controller
@RequestMapping("/educationlevel/")
public class EducationLevelController {

	@Autowired
	private EducationLevelRepository educationLevelRepository;
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("/educationlevel/indexapi");
		return view;
	}
}
