package com.miniproject.demo.controller;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.demo.model.EducationLevel;
import com.miniproject.demo.repository.EducationLevelRepository;


@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiEducationLevelController {
	
	@Autowired
	public EducationLevelRepository educationLevelRepository;
	
	@GetMapping("educationlevel")
	public ResponseEntity<List<EducationLevel>> getAllEducationLevel(){
		try {
			List<EducationLevel> educationLevel = this.educationLevelRepository.findByIsDelete(false);
			return new ResponseEntity<>(educationLevel, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("educationlevel/{id}")
	public ResponseEntity<List<EducationLevel>> getEducationLevelById(@PathVariable("id") BigInteger id) {
		try {
			Optional<EducationLevel> educationLevel = this.educationLevelRepository.findById(id);
			if (educationLevel.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(educationLevel, (HttpStatus.OK));
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<List<EducationLevel>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("add/educationlevel")
	public ResponseEntity<Object> saveEducationLevel(@RequestBody EducationLevel educationLevel) {
		BigInteger add = new BigInteger("1");
		educationLevel.setCreatedBy(add);
		educationLevel.setCreatedOn(Date.from(Instant.now()));
		educationLevel.setIsDelete(false);

		EducationLevel educationLevelData = this.educationLevelRepository.save(educationLevel);
		if (educationLevelData.equals(educationLevel)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("edit/educationlevel/{id}")
	public ResponseEntity<Object> editEducationLevel(@PathVariable("id") BigInteger id, @RequestBody EducationLevel educationLevel) {
		Optional<EducationLevel> educationLevelData = this.educationLevelRepository.findById(id);

		if (educationLevelData.isPresent()) {
			BigInteger edit = new BigInteger("1");
			educationLevel.setId(id);
			educationLevel.setModifyBy(edit);
			educationLevel.setIsDelete(educationLevelData.get().getIsDelete());
			educationLevel.setModifyOn(Date.from(Instant.now()));
			educationLevel.setCreatedBy(educationLevelData.get().getCreatedBy());
			educationLevel.setCreatedOn(educationLevelData.get().getCreatedOn());
			this.educationLevelRepository.save(educationLevel);
			return new ResponseEntity<Object>("Update Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
	@PutMapping("delete/educationlevel/{id}")
	public ResponseEntity<Object> deleteEducationLevel(@PathVariable("id") BigInteger id) {
		Optional<EducationLevel> educationLevelData = this.educationLevelRepository.findById(id);

		if (educationLevelData.isPresent()) {
			BigInteger delete = new BigInteger("1");
			EducationLevel educationLevel = new EducationLevel();
			educationLevel.setId(id);
			educationLevel.setIsDelete(true);
			educationLevel.setModifyBy(delete);
			educationLevel.setModifyOn(Date.from(Instant.now()));
			educationLevel.setCreatedBy(educationLevelData.get().getCreatedBy());
			educationLevel.setCreatedOn(educationLevelData.get().getCreatedOn());
			educationLevel.setName(educationLevelData.get().getName());
			this.educationLevelRepository.save(educationLevel);
			return new ResponseEntity<>("Delete Successfully", HttpStatus.OK);

		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PostMapping("educationlevel/search/")
	public ResponseEntity<List<EducationLevel>> getEducationLevelName(@RequestParam("keyword") String keyword) {
		if (keyword.equals("")){
			List<EducationLevel> educationLevel = this.educationLevelRepository.findByIsDelete(false);
			return new ResponseEntity<List<EducationLevel>>(educationLevel, HttpStatus.OK);
		} else {
			List<EducationLevel> educationLevel = this.educationLevelRepository.searchByKeyword(keyword);
			return new ResponseEntity<List<EducationLevel>>(educationLevel, HttpStatus.OK);
		}
	}


}
